name := "TwitterBoard"

version := "1.0"

scalaVersion := "2.12.1"

// Database
libraryDependencies += "org.mongodb" %% "casbah" % "3.1.1"

// Http client
libraryDependencies += "com.softwaremill.sttp" %% "core" % "1.1.12"

// Spring
lazy val springVersion = "1.5.3.RELEASE"
libraryDependencies += "org.springframework.boot" % "spring-boot-starter-web" % springVersion

// JSON
libraryDependencies +=  "org.json4s" %% "json4s-jackson" % "3.6.0-M2"

// Utils
libraryDependencies += "codes.reactive" %% "scala-time" % "0.4.1"

// Test
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "org.mockito" % "mockito-all" % "1.9.5"
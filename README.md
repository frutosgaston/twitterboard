# TwitterBoard

Frontend TODO list:

- Manejar los casos de error de las request.

- Hacer que los logeos y registros no sean hardcodeados (usar algo como auth0 en lo posible).

- Saber qué interés tengo seleccionado.

- Poder borrar intereses.

- Quiero poder mandar requests con # en el parametro.

- Testear.

- Integrar CI

- Un index en donde pueda ver todos los tweets de todos los intereses al mismo tiempo.


Backend TODO list:
- Manejar los casos de error en lugares clave como DAOs y Controllers.

- Encapsular en otro lado datos hardcodeados como la url de la db.

- Arreglar el nombre de la colección en la db. (El nombre de la clase no se está tomando correctamente)

- Testear más. (Varias partes no están testeadas, como la API.)

- Integrar CI

- Que el id de los usuarios deje de ser el username, debería ser un número autogenerado.

- Investigar por qué twitter envía hasta 15 tweets con un tiempo limitado.

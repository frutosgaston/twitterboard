import com.daos._
import org.scalatest._
import scala.models._

trait Builder extends BeforeAndAfterEach { this: Suite =>

  val genericDAO = new MongoGenericDAO[User]

  override def afterEach() {
    try super.afterEach() // To be stackable, must call super.afterEach
    finally genericDAO.deleteAll
  }
}

class UserDAOSpec extends FlatSpec with Builder with Matchers {
  val dao = new UserDAO

  "When we save a user in the db and we get it back" should "get an identical one" in {
    val user = createUser
    dao.save(user)

    val dbUser = dao.get(user.username)
    dbUser.username shouldEqual user.username
    dbUser.interests shouldEqual user.interests
  }

  "When we delete a created user in the db" should "get it back and it shouldnt be available anymore" in {
    val user = createUser
    dao.save(user)

    val deletedUser = dao.delete(user.username)
    deletedUser.username shouldEqual user.username
  }

  "When we save a user two times with different interests" should "have the second interests" in {
    val user = createUser
    val firstInterests = user.interests

    dao.save(user)

    val dbUser = dao.get(user.username)
    dbUser.interests.length shouldEqual firstInterests.length

    user.interests = "#messi" :: firstInterests
    dao.update(user)

    val dbUser2 = dao.get(user.username)
    dbUser2.interests.length shouldEqual firstInterests.length + 1
  }

  def createUser:User = {
    val user = new User("gfrutos")
    user.interests = List("aaa", "eee")
    user
  }
}
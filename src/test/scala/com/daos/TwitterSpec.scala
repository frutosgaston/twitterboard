import org.scalatest._

import scala.daos.Twitter
import scala.models.Post

class TwitterSpec extends FlatSpec with Matchers {
  val twitter = new Twitter

  "When we try to get tweets from a hastag" should "get a list of posts that contains it" in {
    val posts:List[Post] = twitter.getPosts("#messi")

    posts.head.message.toLowerCase should include ("#messi")
  }

  "When we try to get tweets from a user" should "get a list of posts that contains it" in {
    val posts:List[Post] = twitter.getPosts("@liomessi")
    val firstPost = posts.head

    firstPost.message.toLowerCase + firstPost.writerUser.toLowerCase should include ("liomessi")
  }

  "When we try to get tweets from a user that does not exists" should "get an empty list" in {
    val posts:List[Post] = twitter.getPosts("@twitterboardfaketweet")

    posts should have length 0
  }

  "When we try to get tweets with no interest" should "get an empty list" in {
    val posts:List[Post] = twitter.getPosts("")

    posts should have length 0
  }

}
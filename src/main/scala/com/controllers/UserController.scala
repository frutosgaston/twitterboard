package com.controllers

import javax.validation.Valid

import com.exception.BadRequestException
import org.springframework.validation.BindingResult
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation._
import com.services.{ServiceFactory, UserService}
import com.utils.JavaUser

import collection.JavaConversions._

@CrossOrigin
@RestController
class UserController {
  val serviceFactory: ServiceFactory = new ServiceFactory
  val userService: UserService = serviceFactory.getUserService

  @RequestMapping(value = Array("/user/{id}"), method = Array(RequestMethod.GET))
  def getUser(@PathVariable id: String): JavaUser = userService.get(id).toJava

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  def createUser(@RequestBody user: JavaUser): JavaUser = {
    userService.save(user.toUser).toJava
  }

  @RequestMapping(value = Array("/user/{id}"), method = Array(RequestMethod.PATCH))
  @ResponseStatus(HttpStatus.OK)
  def updateUser(@PathVariable id: String, @Valid @RequestBody user: JavaUser, bindingResult: BindingResult): JavaUser = {
    if (bindingResult.hasErrors) throw BadRequestException(bindingResult.getFieldError.getDefaultMessage) // Patente pendiente
    userService.update(id, user.toUser).toJava
  }

  @RequestMapping(value = Array("/user/{id}"), method = Array(RequestMethod.DELETE))
  @ResponseStatus(HttpStatus.NO_CONTENT)
  def deleteUser(@PathVariable id: String): JavaUser = userService.delete(id).toJava
}

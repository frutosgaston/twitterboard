package com.controllers

import com.services.ServiceFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation._
import collection.JavaConversions._
import scala.models.Post

@CrossOrigin
@RestController
class PostController {

  @RequestMapping(value = Array("/posts"), method = Array(RequestMethod.GET))
  @ResponseStatus(HttpStatus.OK)
  def searchPosts(@RequestParam("q") q: String): java.util.List[Post] = {
    new ServiceFactory().getPotmanInstance.getPosts(q)
  }

}

package scala.models

import com.utils.JavaUser
import scala.collection.JavaConversions._

class User (var username:String, var interests:List[String]) {

  def this(username:String) {
    this(username, List())
  }

  def toJava = {
    new JavaUser(username, interests)
  }

}
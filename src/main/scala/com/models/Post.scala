package scala.models

import scala.beans.BeanProperty

class Post(@BeanProperty var pageId:String, @BeanProperty var createdAt:String, @BeanProperty var message:String,
           @BeanProperty var writerName:String, @BeanProperty var writerUser:String) {}

package scala.daos

import com.softwaremill.sttp._
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.models.Post

class Twitter {
  private val baseUrl: String = "https://api.twitter.com"
  private implicit val backend = HttpURLConnectionBackend()
  implicit val formats = org.json4s.DefaultFormats

  def getPosts(interest: String): List[Post] = {
    val searchUrl: String = s"$baseUrl/1.1/search/tweets.json?"
    val response = search(searchUrl, interest)
    val jsonResponse = parseResponse(response.body)
    parsePosts(jsonResponse)
  }

  private def parsePosts(jsonResponse: JValue) = {
    val postsInfo = postsData(jsonResponse)
    val amountOfPosts = postsInfo("ids").length
    List.tabulate(amountOfPosts) { n =>
      new Post(postsInfo("ids")(n), postsInfo("createdDates")(n), postsInfo("texts")(n),
               postsInfo("names")(n), postsInfo("userNames")(n))
    }
  }

  private def postsData(json :JValue): Map[String, List[String]] = {
    val statuses = json \ "statuses"
    val writer = statuses \ "user"
    Map(
      "ids" -> digAndParse(statuses, "id_str"),
      "createdDates" -> digAndParse(statuses, "created_at"),
      "texts" -> digAndParse(statuses, "text"),
      "names" -> digAndParse(writer, "name"),
      "userNames" -> digAndParse(writer, "screen_name")
    )
  }

  private def digAndParse(json:JValue, attribute:String):List[String] = {
    val values = (json \ attribute).values
    if (values != None) values.asInstanceOf[List[String]] else List()
  }

  private def search(url: String, interest: String) = {
    val headers = Map("authorization" -> s"Bearer ${token()}")
    sttp.headers(headers).get(uri"${url}q=$interest").send()
  }

  private def token(): String = {
    val base64 = "T2hIT0FEdHV4aDNXbkpRSzBBd3U2MlFyMDpkeFp6Yk1GOHpFbFY2RnUzMHJJOFUzMUtBdEpGaUxRMDBhSUJwY0kyY1NGRTFpY2tCdQ=="
    var body = "grant_type=client_credentials"
    var headers = Map("authorization" -> s"Basic $base64", "accept" -> "*/*", "Content-Type" -> "application/x-www-form-urlencoded;charset=UTF-8.")
    val url = uri"$baseUrl/oauth2/token"
    val response: Id[Response[String]] = sttp.headers(headers).body(body).post(url).send()
    parseToMap(response.body)("access_token")
  }

  private def parseToMap(jsonStr:Either[String, String]): Map[String, String] = {
    parseResponse(jsonStr).extract[Map[String, String]]
  }

  private def parseResponse(response:Either[String, String]) = {
    val responseString = response.fold(l => l, r => r.toString)
    parse(responseString)
  }
}
package com.daos

import com.mongodb.{BasicDBList, DBObject}
import com.mongodb.casbah.commons.MongoDBObject

import scala.models.User

// Falta manejar los casos de error.
class UserDAO {

  val mongoDAO = new MongoGenericDAO[User]

  def get(username:String):User = {
    val dbObject:DBObject = mongoDAO.get("username", username)
    parseUser(dbObject)
  }

  def save(user:User):User = {
    val dbObject = MongoDBObject("username" -> user.username, "interests" -> user.interests)
    mongoDAO.save(dbObject)
    user
  }

  def delete(username:String) = {
    val dBObject = MongoDBObject("username" -> username)
    val dbUser = mongoDAO.remove(dBObject)
    parseUser(dbUser)
  }

  def update(user:User) = {
    val query:DBObject = mongoDAO.get("username", user.username)
    val newObject = MongoDBObject("username" -> user.username, "interests" -> user.interests)
    mongoDAO.update(query, newObject)
    user
  }

  private def parseUser(dbObject: DBObject):User = {
    val user = new User(dbObject.get("username").asInstanceOf[String])
    val interestsDBList = dbObject.get("interests").asInstanceOf[BasicDBList]
    user.interests = parseInterests(interestsDBList)
    user
  }

  private def parseInterests(dbList: BasicDBList):List[String] = {
    val array = dbList.toArray
    List(array: _*).map(_.asInstanceOf[String])
  }

}

package com.daos

import scala.reflect.runtime.universe.{TypeTag, typeOf}
import com.mongodb.casbah.Imports._

class MongoGenericDAO[A] {
  val dbName:String = "twitterBoardDb"
  val mongoCollection = getCollectionFor(className)

  def deleteAll = {
    mongoCollection.drop()
  }

  def save(element: DBObject): Unit = {
    mongoCollection.insert(element)
  }

  def get(idName: String, id: String): DBObject = {
    val query = MongoDBObject(idName -> id)
    mongoCollection.findOne(query)
  }

  def remove(dBObject: DBObject) = {
    mongoCollection.findAndRemove(dBObject)
  }

  def update(query: DBObject, newObject: DBObject) = {
    mongoCollection.update(query, newObject)
  }

  private def className[A: TypeTag] = typeOf[A].typeSymbol.name.toString
  private def getCollectionFor(entityType: String) = {
    val database = openConnection
    database.getCollection(entityType)
  }
  private def openConnection:MongoDB  = {
    val mongoClient = MongoClient("localhost")
    mongoClient(dbName)
  }
}
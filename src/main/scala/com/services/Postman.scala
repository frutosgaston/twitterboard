package scala.services

import scala.daos.Twitter
import scala.models.Post

class Postman(newDao:Twitter) {
  val dao = newDao

  def getPosts(interest:String): List[Post] = {
    this.dao.getPosts(interest)
  }

}
package com.services

import com.daos.UserDAO
import scala.models.User

class UserService(dao:UserDAO) {
  val userDao: UserDAO = dao

  def get(username: String): User = {
    userDao.get(username)
  }

  def delete(username: String): User = {
    userDao.delete(username)
  }

  def update(username: String, user: User): User = {
    userDao.update(user)
  }

  def save(user: User): User = {
    userDao.save(user)
  }
}

package com.services

import com.daos.UserDAO

import scala.daos.Twitter
import scala.services.Postman

class ServiceFactory {

  def getPotmanInstance = { new Postman(new Twitter) }
  def getUserService = { new UserService(new UserDAO) }

}

package com.utils

import java.util

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.models.User

// Helper class to let Jackson parse User class.
class JavaUser(@BeanProperty var username:String, @BeanProperty var interests: java.util.List[String]) {

  def this(username:String) {
    this(username, new util.ArrayList[String]())
  }

  def this() {
    this("", new util.ArrayList[String]())
  }

  def toUser: User = {
    new User(username, interests.toList)
  }

}